var mongoose = require('mongoose');

//Books Schema

var bookSchema = mongoose.Schema({
    name:{
        type: String,
        required:true
    },
    patronymic:{
        type: String
    },
    genre:{
        type: String,
        required:true
    },
    surname:{
        type: String,
        required:true
    },
    sex:{
        type: String
    },
    phone:{
        type: String
    },
    work_start:{
        type: String
    },
    work_stop:{
        type: String
    },
    create_date:{
        type: Date,
        default: Date.now
    },
});
var Book = module.export = mongoose.model('Book', bookSchema);

//GET books
module.exports.getBooks = function(callback, limit) {
    Book.find(callback).limit(limit);
};

//Get book
module.exports.getBookById = function(id, callback) {
    Book.findById(id, callback);
};

//ADD book
module.exports.addBook = function(book, callback) {
    Book.create(book, callback);
};

//Update Book
module.exports.updateBook = function (id, book, options, callback) {
    var query = {_id: id};
    var update = {
        name:book.name,
        patronymic:book.patronymic,
        genre:book.genre,
        surname:book.author,
        sex:book.sex,
        phone:book.phone,
        work_start:book.work_start,
        work_stop:book.work_stop
    };
    Book.findOneAndUpdate(query, update, options, callback);
};


//Delete Book
module.exports.removeBook = function(id,callback) {
    var query = {_id: id};
    Book.remove(query, callback);
};